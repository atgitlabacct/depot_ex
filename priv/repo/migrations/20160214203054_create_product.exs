defmodule DepotEx.Repo.Migrations.CreateProduct do
  use Ecto.Migration

  def change do
    create table(:products) do
      add :title, :string
      add :description, :text
      add :price, :float
      add :image_url, :string

      timestamps
    end

  end
end
