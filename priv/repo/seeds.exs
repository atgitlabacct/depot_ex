# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     DepotEx.Repo.insert!(%DepotEx.SomeModel{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias DepotEx.Repo
alias DepotEx.Product

products = [
  %Product{
    title: "Fantastic Beasts and Where to Find Them",
    description: "A copy of FANTASTIC BEASTS & WHERE TO FIND THEM resides in almost every wizarding household in the country. Now Muggles too have the chance to discover where the Quintaped lives, what the Puffskein eats, and why it is best not to leave milk out for a Knarl.",
    price: 7.40,
    image_url: "http://www.amazon.com/Fantastic-Beasts-Where-Harry-Potter/dp/0545850568/ref=pd_bxgy_14_2?ie=UTF8&refRID=0W5PERWBCFFTMSTV3ZB0"
  },
  %Product{
    title: "Quidditch Through the Ages",
    description: "The most checked-out book in the Hogwarts Library, and a volume no Quidditch player or Harry Potter fan should be without!",
    price: 6.74,
    image_url: "http://www.amazon.com/Quidditch-Through-Ages-Harry-Potter/dp/0545850584/ref=pd_bxgy_14_img_2?ie=UTF8&refRID=0XWFH368D3WBY6JA645F"}
]

Enum.each products, &(Repo.insert &1)
