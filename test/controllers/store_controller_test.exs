defmodule DepotEx.StoreControllerTest do
  use DepotEx.ConnCase

  alias DepotEx.Repo
  alias DepotEx.Product

  test "GET /", %{conn: conn} do
    Repo.insert(%Product{title: "Chamber of Secrets",
      description: "Harry Potter",
      price: 7.00,
      image_url: nil})
    conn = get conn, "/"
    assert html_response(conn, 200) =~ "Chamber of Secrets"
  end

end
