defmodule DepotEx.StoreController do
  use DepotEx.Web, :controller

  def index(conn, _params) do
    products = DepotEx.Repo.all DepotEx.Product
    render conn, "index.html", products: products
  end
end
