defmodule DepotEx.Product do
  use DepotEx.Web, :model

  schema "products" do
    field :title, :string
    field :description, :string
    field :price, :float
    field :image_url, :string

    timestamps
  end

  @required_fields ~w(title description price image_url)
  @optional_fields ~w()

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end
end
